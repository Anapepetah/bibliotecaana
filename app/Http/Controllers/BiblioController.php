<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Livros;
use App\Models\Autores;
use App\Models\Editoras;

class BiblioController extends Controller
{
    public function lilivros(){

        $livros=Livros::all();
        return view('Listagem/listagemLivros', ['livros'=>$livros]);
    }

    public function editoras(){
        $editoras=Editoras::all();
        return view('Listagem/listagemEditoras', ['editoras'=>$editoras]);

    }

    public function autores(){
        $autores=Autores::all();
        return view('Listagem/listagemAutores', ['autores'=>$autores]);

    }

    public function editarautores(){
        $autores=Autores::all();
        return view('Editar/editarAutores', ['autores'=>$autores]);

    }
    public function editareditoras(){
        $editoras=Editoras::all();
        return view('Editar/editarEditoras', ['editoras'=>$editoras]);

    }
    public function editarlivros(){
        $livros=Livros::all();
        return view('Editar/editarLivros', ['livros'=>$livros]);

    }
}
